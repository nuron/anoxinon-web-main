+++
date = "2019-05-09T12:20:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #03"
description = "Aktuelles vom Kreuzer"
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["Vereinsangelegenheiten", "Website", "Verein"]

+++
Hallo zusammen,

wieder ist ein Monat rum und es wird Zeit für uns zurückzuschauen.  
Ohne Großes drum und dran, fangen wir gleich an! ;)

----
###### Finanzen & Transparenz

Uns ist es wichtig möglichst transparent zu sein. Aus diesem Grund haben wir beschlossen, in Zukunft, regelmäßig Informationen über die aktuellen Kosten und die Nachhaltigkeit der Finanzierung auf unserer Website bereitzustellen.

Soviel sei gesagt: Der Betrieb der aktuell verfügbaren Dienste ist ohne größere Probleme möglich. Allerdings ist derzeit für Aktionen und Angebote, abseits der Bereitstellung von Diensten, nicht viel übrig.

---
###### Website

Im letzten Blog Beitrag haben wir den Website Umbau erwähnt. Es freut uns euch mitteilen zu
können, dass wir einen fähigen Webdesigner mit an Board holen konnten. Er hat bereits mit der Arbeit an der
Anoxinon.de Website begonnen und macht gute Fortschritte. Derzeit wird noch am Aussehen gefeilt und im Anschluss daran an den textlichen Inhalten und dem weiteren Feintuning. Wenn alles gut läuft, werden wir noch im nächsten Quartal unsere Website in einem neuen Glanz präsentieren können.

---
###### Satzungsänderungen

Vergangenen Montag waren wir beim Notar vorstellig und haben unsere Satzung eintragen lassen. Das Finanzamt hat, im Rahmen einer Vorabprüfung, keine Beanstandungen gehabt. Die neue Version der Satzung könnt Ihr [hier](/files/Satzung.pdf) abrufen. Eine kurze Übersicht über die Änderungen findet Ihr im [Logbuch #1](/blog/logbuch01/).

---
###### FSFE

Seit kurzem sind wir auf der "[Public Money? Public Code!](https://publiccode.eu/de/)" Seite der FSFE, als nationale Organisation,
gelistet. Wie beschlossen, setzen wir damit ein Statement für Freie Software. Die Initiative will, dass
Software, die durch Steuergelder finanziert wird, auch der Öffentlichkeit frei zur Verfügung steht. Das
Vorhaben birgt einige Vorteile, unter anderem Transparenz und mehr Sicherheit für die Bevölkerung. Alles
weitere könnt Ihr diesem [Video](https://download.fsfe.org/videos/pmpc/pmpc_de_mobile.webm) entnehmen. Die FSFE hat uns gebeten, diese Petition weiterzuverteilen.
Wenn Ihr also dieses Ziel unterstützen wollt, so unterzeichnet sie bitte auch persönlich!


---

Soweit alles Wichtiges.   
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
