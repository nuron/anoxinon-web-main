+++ 
date = "2020-08-14T20:00:00+01:00" 
draft = false 
author = "Anoxinon" 
title = "Zukünftig gemeinnützig bleiben - ja oder nein?" 
description = "Aktuelles" 
categories = ["allgemeines", "verein", "dienste"] 
tags = ["Vereinsangelegenheiten", "Verein"]
+++

Hallo zusammen!

Heute brauchen wir eure Meinung zum Thema Gemeinnützigkeit des Anoxinon e.V.

Uns erreichen immer wieder Anfragen zu weiteren Diensten, wie z.B. Peertube, Nextcloud oder DNS/DDNS.

Als gemeinnütziger Verein profitieren wir von verschiedenen Vorteilen, jedoch sind wir auch sehr stark an den rechtlichen Vereinszweck gebunden. Dies führt dazu, dass wir eine Bringschuld gegenüber dem Finanzamt haben und damit auch ein gewisser Druck einhergeht. Damit können wir personell recht gut umgehen. Jedoch können wir Dienste wie Peertube oder DDNS nicht ohne Weiteres bereitstellen, da dies nicht direkt dem Vereinszweck und der damit bescheinigten Gemeinnützigkeit entspräche.

Um Angebote öffentlich bereitstellen zu können, müssten mehrere Voraussetzungen erfüllt sein:

* Der Dienst muss explizit dem Vereinszweck bezüglich Gemeinnützigkeit entsprechen
* Ausreichend personelle Kapazitäten für Administration und Moderation
* Finanzierung - Speicher und Server kosten Geld

Nachfolgend erhaltet ihr einen Überblick der Vor- und Nachteile diesbezüglich.

---

###### Vorteile der Gemeinnützigkeit

* Image: gemeinnützig ist in der öffentlichen Wahrnehmung ein Vorteil
* Steuererleichterungen (keine Körperschaftssteuer)
* Spenden können steuerlich geltend gemacht werden

---

###### Nachteile der Gemeinnützigkeit

* Strikte Vorgaben für Themen/Dienste um als gemeinnützig anerkannt zu bleiben
* Rechenschaft gegenüber dem Finanzamt führt zu etwas Druck
* Die Mittelverwendung ist stark reglementiert

---

###### Überlegungen

Ob gemeinnützig oder nicht, der Vereinszweck bleibt unter allen Umständen erhalten! Auf der einen Seite ist die Gemeinnützigkeit eine Auszeichnung für uns, auf der anderen Seite aber auch sehr restriktiv.

Eure Meinung hierzu ist uns wichtig! Nehmt euch daher bitte kurz Zeit für die Umfrage auf Mastodon.

---

Vielen Dank für eure Unterstützung!

Team Anoxinon