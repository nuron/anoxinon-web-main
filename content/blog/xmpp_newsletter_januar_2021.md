+++ 
date = "2021-02-20T18:20:00+01:00" 
draft = false 
author = "Jeybe, Arno" 
title = "XMPP Newsletter Januar 2021" 
description = "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Januar 2021" 
categories = ["Community", "Inhalte", "Open Source"] 
tags = ["XMPP", "Messenger", "XSF", "XMPP Newsletter"]
+++ 

---

**Anmerkung:**

*[Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/01/newsletter-01-january/).  Übersetzung und Korrektur der dt. Version von Jeybe und Arno.*

---

## Vorwort

Willkommen zum XMPP-Newsletter für den Monat Dezember 2020 und Januar 2021.

Toll, dass Du wieder vorbeischaust, um unseren aktuellsten Newsletter zu lesen! Wir hoffen auf ein großartiges Jahr für Dich, genauso wie für XMPP!

Im Dezember 2020 haben wir das letzte Jahr der Erstellung und Veröffentlichung des XMPP-Newsletters besprochen, [die Notizen dazu gibt es hier (EN)](https://github.com/xsf/xmpp.org/pull/851#issuecomment-754919811).

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Menschen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/2021/01/newsletter-01-january/)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news/jabberfr.org/category/newsletter/)

## XSF Ankündigung

Bezüglich der Ankündigung von LetsEncrypt das Root-Zertifikat zu wechseln: Nun werden sie die [Kompatibilität der Zertifikate mit Android Geräten ausbauen (EN)](https://letsencrypt.org/2020/12/21/extending-android-compatibility.html). [Lies den originalen Beitrag (EN)](https://letsencrypt.org/2020/11/06/own-two-feet.html)!

Edward Maurer hat einen Kommentar hinsichtlich der aktuellen Situation zu Instant Messengern und XMPP geschrieben: [Instant Messaging: Es geht nicht um die App](https://xmpp.org/2021/01/instant-messaging-es-geht-nicht-um-die-app/). Den Blogbeitrag gibt es ebenso in [Englisch (Original)](https://xmpp.org/2021/01/instant-messaging-its-not-about-the-app/) und übersetzt in [Französisch](https://xmpp.org/2021/01/messagerie-instantanee-il-ne-sagit-pas-de-lapplication/), [Spanisch](https://xmpp.org/2021/01/mensajeria-instantanea-no-se-trata-de-la-aplicacion/) und [Rumänisch](https://xmpp.org/2021/01/mesagerie-instantanee-nu-este-vorba-despre-aplicatie/). Danke an alle Beitragenden!

## Veranstaltungen

[Berlin XMPP Treffen (virtuell)](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Meeting von XMPP-Enthusiasten in Berlin - immer am zweiten Mittwoch jedes Monats.

## Artikel

Die Entwickler:innen des Prosody XMPP Servers haben in einem Blogbeitrag eine [Rückschau von 2020 (EN)](https://blog.prosody.im/2020-retrospective/) geliefert, in dem sie zusammenfassen, was in dem Projekt das letzte Jahr alles geschehen ist. Der Beitrag liefert einige aufschlussreiche Statistiken über die Anzahl der laufenden Prosody Installationen weltweit.

Wolthera hat einen weiteren Artikel geschrieben, der erklärt, wie man einen [XMPP-Client in Godot (EN)](https://wolthera.info/2020/12/xmpp-client-in-godot/) schreibt.

Die Entwickler:innen von [blabber.im](https://blabber.im/) haben einen Rückblick über die [Statistiken von 2020](https://blabber.im/en/das-jahr-2020-im-rueckblick/) veröffentlicht, die einen interessanten Einblick in die Serverauslastung, die registrierten Nutzer:innen und mehr geben. blabber.im ist das neue Zuhause des Pix-Art-Messengers, einer Conversations-Abspaltung, welche [umbenannt werden musste](https://blabber.im/en/wie-pix-art-messenger-aus-dem-google-play-store-verschwand/), da es rechtliche Komplikationen mit einem ähnlich benannten Unternehmen gab. [Pix-Art Messenger wird nun unter dem Namen blabber.im weitergeführt](https://blabber.im/en/blabber-im-v3-0-0-verfuegbar/), mit den gleichen Funktionen, die schon Pix-Art bot.

Paula Kreuzer hat eine [kreative Stickerei des XMPP-Logos](https://pixelfed.de/p/paulakreuzer/256133901083348992) gemacht!

XMPP Rückschau: Das offene Instant Messaging Protokoll heute & damals. XMPP bleibt ein kritisches Rahmenwerk für Chat und Messaging Applikationen, auch 20 Jahre nach dessen Veröffentlichung. [Hier ein Überblick über die Funktionsweise (EN)](https://getstream.io/blog/xmpp-extensible-messaging-presence-protocol/).

In seinem Blog teilt Dryusdan sein [Rezept (FR)](https://dryusdan.space/installer-son-serveur-xmpp-100-conforme-aux-xep/), um seinen eigenen XMPP-Server auzurollen, mit den folgenden Hauptzutaten: Debian oder Ubuntu, ejabberd und PostgreSQL. Die Anleitung garantiert 100% im bekannten Conversations Compliance Test.

Nico [erklärt warum und wie (FR)](https://linuxfr.org/users/therealnicoco/journaux/ma-passerelle-xmpp-signal), er sein eigenes XMPP/Signal Gateway namens [spectrum2_signald](https://gitlab.com/nicocool84/spectrum2_signald/) erstellt hat. Es ist noch nicht perfekt (nur ein Signal-Konto wird bisher unterstützt), aber es ist quelloffen!

MongooseIM hat einen Artikel über [Sicherheit und Datenschutz (EN)](https://www.erlang-solutions.com/blog/how-to-ensure-your-instant-messaging-solution-offers-users-privacy-and-security/) beim Instant Messaging veröffentlicht.

Tails kommt mit einem XMPP-Client, aktuell Pidgin. [Aus Sicherheitsgründen wird Tails Pidgin ersetzen (EN)](https://fosstodon.org/@tails/105587047791026258). Der neue XMPP-Client wird aller Wahrscheinlichkeit nach [Dino](https://fosstodon.org/@dino) oder [Gajim](https://fosstodon.org/@gajim) werden.

Die Deutsche Zeitschrift "taz" hat XMPP generell und spezifisch Conversations und Quicksy in einem [Artikel über WhatsApp-Alternativen](https://taz.de/Alternative-Messenger-Dienste/!5743214/)  genannt.

["XMPP - was ist das?"](https://www.heise.de/tipps-tricks/XMPP-was-ist-das-5006209.html) Beitrag im Deutschen "Heise Online tipps + tricks" Magazin.

Axel Reimer hat eine Schnellstart-Anleitung für Siskin IM auf seinem Blog veröffentlicht. Er ist in [Deutsch](https://xmppeinrichtung.blogspot.com/2021/01/siskin-im-auf-dem-iphone-als-instant.html) und [Englisch](https://xmppeinrichtung.blogspot.com/2021/01/installation-and-configuration-of.html) verfügbar.

(Etwas zu spät, aber interessant für iOS Entwickler:innen - [Chris Beckstrom erklärt, wie er iMessage und XMPP gekoppelt hat (EN)](https://chrisbeckstrom.com/posts/How-I-Bridged-iMessage-and-XMPP/).)

## Software News

### Clients und Apps

[Conversations 2.9.2 und 2.9.3 (EN)](https://github.com/iNPUTmice/Conversations/blob/master/CHANGELOG.md) wurden freigegeben und bringen die SCRAM-SHA-512-Authentifizierung, sowie Untersützung für einfache Einladungen auf kompatiblen Servern und mehr. Darüber hinaus wurde Conversations 2.9.6 veröffentlicht, mit kleineren Stabilitätsverbesserungen für A/V-Anrufe und einigen Fehlerbehebungen.

[Gajim 1.3.0 Beta1 (DE)](https://gajim.org/de/post/2021-01-01-gajim-1.3.0-beta-released/) und [1.3.0 Beta2 (DE)](https://gajim.org/de/post/2021-01-09-gajim-1.3.0-beta2-released/) wurden freigegeben. Seit der Veröffentlichung von Gajim 1.2.2. sind mehr als vier Monate vergangen. Vieles ist während dieser Zeit passiert, einschließlich einer kompletten Neugestaltung der beiden Gajim-Einstellungs-Fenster und des Konfigurations Backends. Wirf als Erste:r einen Blick auf die neue Beta und hilf uns die letzten Fehler vor der endgültigen Version auszumerzen!

Gajim Entwicklung: [Im Dezember (DE)](https://gajim.org/de/post/2020-12-28-development-news-december/) erhielt das Profilfenster eine komplette Überarbeitung und bietet nun modernsten vCard-Support und einen brandneuen Profilbild Selektor. Darüber hinaus wurden Chat Marker verbessert und einige Fehler behoben. [Im Januar (DE)](https://gajim.org/de/post/2021-01-28-development-news-january/) wurde einige lange erwartete Funktion implementiert: direkte Chat-Nachrichten für private Gruppen-Chats. Außerdem ist [@gajim nun auf Fosstodon (EN)](https://fosstodon.org/@gajim)!

![Gajim hat einen neuen Profilbild-Selektor](/img/blog/xmpp_newsletter_januar_2021/GajimProfile.png "Gajim hat einen neuen Profilbild-Selektor")

[Kaidan wird ein Sponsoring für Ende-zu-Ende-Verschlüsselung erhalten (EN)](https://www.kaidan.im/2021/01/07/end-to-end-encryption/): Kaidan wird von NLnet untsersützt werden, um Ende-zu-Ende Verschlüsselung zu implementieren. Wir werden die aktuellste Version von OMEMO implementieren und diese um ein einfaches Vertrauensmanagement erweitern und so die Fehler lösen, die andere OMEMO-fähige Clients aufweisen. 

[MatriX vNext Entwicklungs Update (EN)](https://www.ag-software.net/2020/12/30/matrix-vnext-development-update/).

[Monal 4.9 für iOS (EN)](https://monal.im/blog/ios-4-9-is-out/) wurde veröffentlicht, mitsamt einer kompletten Überarbeitung des internen XMPP-Parsers, der OMEMO-Implementierung und vielen neuen Funktionen (Chat Marker, Nicknamen, Avatare). [Monal 4.9 für MAC (EN)](https://monal.im/blog/mac-4-9-beta-out-with-apple-silicon-support/) wird bald veröffentlicht und unterstützt nun 'Apple Silicon'. Bei Bedarf kannst Du [Monal nun auch auf Reddit (EN)](https://www.reddit.com/r/monal/) folgen. [Beta-Tester:innen sind aufgefordert, aktiv Feedback über die aktuelle Erfahrung mit Monal zu liefern (EN)](https://monal.im/blog/wanted-monal-feedback/). Außerdem haben die Monal-Entwickler:innen ein Update herausgegeben, das eine [kritische Sicherheitslücke](https://monal.im/blog/cve-2020-26547/) schließt. Zu guter Letzt brauchst Du für das Update zu Monal 5.0 [mindestens iOS 13 (EN)](https://monal.im/blog/monal-5-0-will-be-the-last-version-to-support-ios12/) .

![Monal nimmt Deine Privatsphäre ernst](/img/blog/xmpp_newsletter_januar_2021/monalproductpagepreview.png "Monal nimmt Deine Privatsphäre ernst")

[Movim ist voller neuer Funktionen für 2021 (EN)](https://nl.movim.eu/?node/pubsub.movim.eu/Movim/movim-is-full-of-new-features-for-2021-oyJ4gu)! Lies den neusten Blogbeitrag um zu erfahren, was Du im anstehenden Release erwarten kannst und was kürzlich alles dazukam, wie zum Beispiel URL-Einbindung, das Zwischenspeichern von Bildern und mehr. [Movim hat ebenso die eigene Serverkapazität erhöht (EN)](https://nl.movim.eu/?node/pubsub.movim.eu/Movim/156c9c91-bbe3-45f0-a5df-1016d641ede6), um mit den hunderten von neuen Nutzer:innen einserseits auf den XMPP Diensten movim.eu und jappix.com, aber auch auf Movim selbst klarzukommen. Der neue offizielle Movim Pod ist nun auf [mov.im](https://mov.im) beherbergt und der Rest der Dienste (XMPP, Telegram/Slack/IRC Brücken) sind nun auf dem neuen Server ausgerollt. Ein herzliches Willkommen an all die neuen Nutzer:innen die in den letzten Tagen zu Movim gestoßen sind!

Die Ignite Realtime Community hat [Openfire Pàdé v1.2.2 (EN)](https://discourse.igniterealtime.org/t/openfire-pade-1-2-0-released/89294) und Openfire [Pàdé v1.3.0 (EN)](https://discourse.igniterealtime.org/t/openfire-meetings-becomes-pade/89401) angekündigt und veröffentlicht. Diese Veröffentlichung kombiniert alle Openfire Meeting Module (Jitsi Meet, Videobridge, Focus und das SIP Gateway) und ConverseJS in ein einziges Plugin names Pàdé. Außerdem sind einige Fehlerbehebungen enthalten.

[Profanity 0.10.0 wurde veröffentlicht (EN)](https://github.com/profanity-im/profanity/releases/tag/0.10.0). Das Ziel für diese Version war hauptsächlich internes Aufräumen, aber es gab auch einige Verbesserungen unter der Haube, die MAM ([XEP-0313 (EN)](https://xmpp.org/extensions/xep-0313.html)) betreffen.

[Quicksy.im](https://quicksy.im/), ein Conversations Spin-Off für einen einfachen Einstieg verzeichnet [über 30% Nutzerinnen und Nutzerzuwachs (EN)](https://twitter.com/iNPUTmice/status/1351613901104963592) während der letzten zwei Wochen. Quicksy 2.9.6 kann nun Verifikations-SMS automatisch erkennen und verarbeiten.

[SàT-Fortschrittsbericht 2020-W53 (EN)](https://www.goffi.org/b/LFMqr7xC2aNf4MDgkbamBY/progress-note): Volltextsuche in Pubsub, einfache Einladungen für das Teilen von Dateien und mehr.

[SiskinIM 6.2 wurde veröffentlicht (EN)](https://mastodon.technology/@tigase/105627824140193490), bringt Unterstützung für OMEMO-verschlüsselte Gruppenchats (private Gruppen), und Fehlerbehebungen bei VoIP.

[UWPX v.0.30.0.0 (EN)](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.30.0.0) wurde veröffentlicht und beinhaltet eine experimentelle Implementierung von [XEP-0313 MAM (EN)](https://xmpp.org/extensions/xep-0313.html). Neben der Bereitstellung von [MAM (EN)](https://xmpp.org/extensions/xep-0313.html) wurden bei [UWPX (EN)](https://uwpx.org/) eine Reihe kleiner Fehler behoben und die allgemeine Handhabung  von [XEP-0384 OMEMO (EN)](https://xmpp.org/extensions/xep-0384.html) verbessert. Dies sind nur die Änderungen, die es in das aktuelle Release geschafft haben, in Zukunft wird es viele weitere Neuerungen geben. [COM8](https://github.com/COM8) arbeitet daran, die Implementierung von [XEP-0384 OMEMO (EN)](https://xmpp.org/extensions/xep-0384.html) auf den aktuellen Stand (`0.7.0 (2020-09-05)`) zu bringen. Des weiteren wird gerade die Datenbankschnittstelle komplett überarbeitet (Wechsel von [SQLite-net DB (EN)](https://github.com/praeclarum/sqlite-net) zu [Entity Framework Core (EN)](https://docs.microsoft.com/en-us/ef/core/)).

## Server

[ejabberd 20.12 wurde veröffentlicht (EN)](https://www.process-one.net/blog/ejabberd-20-12/) und enthält etliche neue Funktionen sowie viele Verbesserungen & Fehlerbehebungen (z.B. neue Authentifizierungsmethoden und Datenbanktreiber).

[Openfire 4.6.1 (EN)](https://discourse.igniterealtime.org/t/openfire-4-6-1-is-released/89430) wurde veröffentlicht. Diese Version beinhaltet hauptsächlich Fehlerbehebungen. Sie legt primär den Fokus darauf im Cluster-Betrieb die Funktionalität für Mehrbenutzer:innern-Chat und Pubsub zu verbessern. Darüber hinaus beinhaltet diese Version weitere Verbesserungen, wie z.B. eine Steigerung der LDAP/AD-Performanz. Alle Änderungen, einschließlich der seit Version 4.0.6 behobenen Fehler, sind im [Changelog (EN)](https://download.igniterealtime.org/openfire/docs/latest/changelog.html) beschrieben.

## Bibliotheken

Gajims XMPP-Bibliothek [python-nbxmpp hat ihr 2.0.0-Release (EN)](https://dev.gajim.org/gajim/python-nbxmpp/-/blob/master/ChangeLog) erfahren, mit der [XEP-0106: JID-Escaping](https://xmpp.org/extensions/xep-0106.html), [XEP-0292: vCard4 über XMPP](https://xmpp.org/extensions/xep-0292.html), und [XEP-0233: XMPP-Server-Registriergung mit Kerberos V5](https://xmpp.org/extensions/xep-0233.html) unterstützt wird.

[slixmpp 1.6.0 und 1.7.0 wurden veröffentlicht (EN)](https://lab.louiz.org/poezio/slixmpp/-/releases#slix-1.7.0). Diese Releases bringen Unterstützung für viele neue XEPs (inklusive einiger [MIX-XEPs (EN)](https://xmpp.org/extensions/xep-0369.html)), zusammen mit Fehlerbehebungen und Verbesserungen.

Die Ignite-Realtime-Entwickler:innen  [freuen sich, Smack 4.4.0 anzukündigen (EN)](https://discourse.igniterealtime.org/t/smack-4-4-0-released/89283), welches das erste Haupt-Release ist seit der Veröffentlichung von Smack 4.3.0 im August 2018. Smack 4.4.0 enthält viele neue Funktionen, Fehlerbehebungen und Verbesserungen.

## Erweiterungen und Spezifikationen

Aus der ganzen Welt arbeiten Entwickler:innen und andere Expert:innen des XMPP-Standards zusammen an Erweiterungen, entwickeln neue Spezifikationen für entstehende Anwendungsfälle und verfeinern existierende Lösungen. Jede:r kann dabei Vorschläge einbringen. Die besonders erfolgreichen Eingaben werden am Ende *final* oder *aktiv* - je nach Typ -, während andere sorgsam als *zurückgestellt* archiviert werden. Dieser *Lebenszyklus der Erweiterungen* wird in [XEP-0001 (EN)](https://xmpp.org/extensions/xep-0001.html) beschrieben und enthält die formellen und anerkannten Definitionen für *Typen*, *Zustände* und *Prozesse*. [Lies mehr über den Standardisierungsprozess (EN)](https://xmpp.org/about/standards-process.html). Kommunikation rund um den Standard und seine Erweiterungen findet auf der [Standards-Mailingliste (EN)](https://mail.jabber.org/mailman/listinfo/standards) statt ([Onlinearchiv (EN)](https://mail.jabber.org/pipermail/standards/)).

### Vorgeschlagen

Der XEP-Entwicklungsprozess beginnt damit eine Idee aufzuschreiben und sie an die XMPP-Schriftleiter:in zu übermitteln. Innerhalb von zwei Wochen entscheidet dann der XMPP-Rat, ob der Vorschlag als experimentelles XEP angenommen wird.

-   [Dienstausfallstatus (EN)](https://xmpp.org/extensions/inbox/service-outage-status.html)
    -   Dieses Dokument beschreibt eine XMPP-Erweiterung, welche es einem Dienstleister ermöglicht allen Benutzer:innen (bevorstehende) Wartungsfenster und Ausfälle auf eine semantische Art und Weise zu kommunizieren.

### Neu

-   Version 0.1.0 von [XEP-0454 OMEMO-Medienfreigabe (EN)](https://xmpp.org/extensions/xep-0454.html)
    -   Ein informeller Weg Dateien zu teilen, trotz Einschränkungen seitens OMEMO-Verschlüssellung.
    -   Akzeptiert am 13.01.2021 durch Ratsabstimmung.

-   Version 0.1.0 of [XEP-0453 DOAP-Einsatz in XMPP (EN)](https://xmpp.org/extensions/xep-0453.html)
    -   Diese Spezifikation beschreibt, wie XMPP-Projekte ihre Fähigkeiten in einem eine maschinenlesbaren Format ([DOAP - Description of a Project (EN)](https://github.com/ewilderj/doap/wiki)) zur Verfügung stellen können und wie externe Entitäten damit interagieren können.
    -   Akzeptiert am 13.01.2021 durch Ratsabstimmung.

-   Version 0.1.0 of [XEP-0452 Erwähnungsbenachrichtigungen für Gruppenchats (EN)](https://xmpp.org/extensions/xep-0452.html)
    -   Diese Spezifikation dokumentiert wie Nutzer:innen darüber informiert werden können, dass sie in einem Gruppenchat (MUC) erwähnt werden, in welchem sie gerade selbst nicht teilnehmen.
    -   Akzeptiert am 06.01.2021 durch Ratsabstimmung.

-   Version 0.1.0 of [XEP-0451 Stanza-Multiplexing (EN)](https://xmpp.org/extensions/xep-0451.html)
    -   Diese Spezifikation stellt einen Mechanismus bereit, um mehrere virtuelle Hosts in einer einzelnen XMPP-Sitzung zu bündeln.
    -   Akzeptiert am 09.12.2020 durch Ratsabstimmung.

-   Version 0.1.0 of [XEP-0450 Automatische Vertrauensverwaltung (EN)](https://xmpp.org/extensions/xep-0450.html)
    -   Dieses Dokument spezifiziert einen Weg, *Vertrauen* in öffentliche Langzeitschlüssel - welche bei Ende-zu-Ende-Verschlüsselung zum Einsatz kommen - zu verwalten.
    -   Akzeptiert am 02.12.2020 durch Ratsabstimmung.

### Zurückgestellt

Wenn ein experimentelles XEP nach mehr als sechs Monaten nicht aktualisiert wurde, wird es automatisch aus *Experimentell* nach *Zurückgestellt* verschoben. Erhält das XEP ein neues Update, wird es wieder nach *Experimentell* verschoben.

-   Diesen Monat wurden keine XEPs zurückgestellt.

### Aktualisiert

-   Version 1.1.0 von [XEP-0393 Nachrichtengestaltung (EN)](https://xmpp.org/extensions/xep-0393.html)
    -   Klarstellung der Verarbeitungsregeln für *span*-Direktiven (sw)

-   Version 1.1 of [XEP-0176 Jingle-ICE-UDP- Transportmethode (EN)](https://xmpp.org/extensions/xep-0176.html)
    -   Das 'foundation'-Attribut wird zu Datentyp *String* statt *unsignedByte*.
    -   Das 'network' Attribut wird optional, und eine Zuordnung zu SDP hinzugefügt (egp)

-   Version 0.4.0 und 0.5.0 der [XEP-0372 Referenzen (EN)](https://xmpp.org/extensions/xep-0372.html)
    -   Es wurde spezifiziert, dass sich die Bereichangaben auf die Unicode-Zeichen beziehen. (kis)
    -   Es wurde spezifiziert, dass das *begin*-Attribut inklusiv ist und bei Null startet sowie dass das *end*-Attribut exklusiv ist (Dijkstra-basierte Konvention) (gh/jcbrand)

-   Version 0.2.0 und 0.3.0 von [XEP-0434 Vertrauensnachrichten (EN)](https://xmpp.org/extensions/xep-0434.html)
    -   Die Nutzung von Vertrauensnachrichten von Protokollen wie der automatische Vertrauensverwaltung wird klargestellt ([XEP-0450 (EN)](https://xmpp.org/extensions/xep-0450.html))
    -   Den Namensraum 'urn:xmpp: atm:0' der automatischen Vertrauensverwaltung (ATM) ([XEP-0450 (EN)](https://xmpp.org/extensions/xep-0450.html)) als Beispiel für das 'usage' Attribut verwenden
    -   Absatz 'Security Considerations' hinzufügen
    -   Verbesserungen bei Erklärungen, Beschreibungen und Beispielen, der Einführung von neuen Attributen und die Finalisierung von allen Abschnitten:
    -   Link zu Verschlüsselungsprotokollnamensräumen entfernen
    -   Kurzen Namen hinzufügen
    -   Einleitung kürzen und verbessern
    -   Betonende Textformatierung anstatt von Gänsefüßchen verwenden
    -   Neuen Abschnitt zur Erklärung der Grundprinzipien von Vertrauensnachrichten einfügen
    -   Beispiele und Vergleiche zwischen Vertrauensnachrichten und öffentlichen Schlüsseln bzw. Zertifikaten hinzugefügt
    -   Beschreibung der Struktur von Vertrauensnachrichten verbessert
    -   'usage' Attribut für das 'trust message' Element hinzgefügt
    -   Fokus auf und Anpassen von Beispielen
    -   Abschnitte 'IANA Considerations', 'XMPP Registrar Considerations' und 'XML Schema' komplettieren. (melvo)

### Letzter Aufruf

Letzte Aufrufe werden einmalig ausgesprochen, sobald alle mit dem aktuellen Status eines XEPs einverstanden sind. Nachdem der Rat entschieden hat, dass ein XEP bereit ist, startet der Schriftleiter einen *letzten Aufruf*. Das Feedback, welches in diesem letzten Aufruf gesammelt wird, soll helfen das XEP zu verbessern, bevor es dem Rat zurückgegeben wird, um es zu einem *Entwurf* zu machen.

-   [XEP-0381 (EN)](https://xmpp.org/extensions/xep-0381.html) Internet der Dinge Sonderinteressensgruppe (IoT SIG)
-   [XEP-0429 (EN)](https://xmpp.org/extensions/xep-0429.html) Sonderinteressensgemeinschaft Ende-Zu-Ende-Verschlüsselung

### Entwürfe

-   Keine Entwürfe diesen Monat.

### Aufruf zur Einreichung von Erfahrungen

Ein Aufruf zur Einreichung von Erfahrungen ist wie ein *letzter Aufruf*, ein expliziter Aufruf Kommentare einzureichen. Dieser Aufruf richtet sich dabei hauptsächlich an Leute, welche die entsprechende Spezifikation bereits implementiert und idealerweise auch ausgeliefert haben. Der Rat stimmt danach ab die Spezifikation nach *Final* zu verschieben.

-   Diesen Monat gab es keine Aufrufe zur Einreichung von Erfahrungen.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Axel Reimer, COM8, deuill, emus, horazont, Licaon_Kter, melvo, pitchum, SamWhited, wurstsalat3000 und zash für ihre Hilfe ihn zu erstellen!

Bis nächstes Jahr, bleibt gesund und munter!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/) aus.

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Ausgabe direkt in Dein Postfach, sobald sie veröffentlicht wird.

## Hilf uns diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters mit dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) angefangen, aber auch ein Issue im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) genutzt. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.


Du hast ein Projekt und möchtest darüber schreiben? Denk' doch darüber nach, Neuigkeiten und Veranstaltungen in unserem Newsletter einem breiten Publikum anzukündigen! Schon ein Beitrag von wenigen Minuten würde uns helfen!

Aufgaben, die regelmäßig erledigt werden müssen wären zum Beispiel:

- Neuigkeiten des XMPP-Universums zusammenfassen
- Kurze Formulierungen zu Nachrichten und Veranstaltungen anfertigen
- Zusammenfassung der monatlichen Kommunikation über Erweiterungen (XEPs)
- Gegenlesen des Entwurfes des XMPP Newsletters
- Vorbereitung von Medien und Bildern
- Übersetzungen, vor allem ins Deutsche und Spanische

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA-Lizenz](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

Veröffentlich von [emus](https://xmpp.org/author/emus.html) am [05. Januar 2021](https://xmpp.org/2021/01/newsletter-01-january/#) - abgelegt unter [Newsletter](https://xmpp.org/category/newsletter.html)

