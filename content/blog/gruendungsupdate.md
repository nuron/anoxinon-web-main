+++
date = "2018-11-06T20:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Gründungsupdate"
description = "Vereinsgründung, Änderungen und Pläne"
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["anoxinon","fediverse","XMPP", "Messenger", "Website"]


+++
Hallo zusammen,  
heute grüßt die Küche mit Brand frischen Neuigkeiten zum Verein, sowie mit  Informationen bezüglich unserer weiteren Pläne.

---

###### Verein – Ziele und Pläne

Vor etwa drei Monaten haben wir uns dazu entschlossen, aus unserem Privaten Hobby Projekt einen Verein zu machen und den Wirkungsbereich zu erweitern. Die Gründe hierfür waren vielschichtig, dazu gehörten unter anderem die Aspekte der Rechtssicherheit (Haftung etc.), der Stabilität unserer Dienste und der Finanzierung. Kurz nach dem ersten Gedankenspiel arbeiteten wir unsere ersten Version der Satzung aus. Da das erklärte Ziel von Anfang an eine „Gemeinnützigkeit“ war, sendeten wir unseren Entwurf zur Vorabprüfung an das Finanzamt. Das nächste Problem, welches sich uns entgegen stellen sollte, war sieben Leute zu finden um die Anforderungen für einen „eingetragenen Verein“ zu erfüllen. Dort schossen wir gleich über das Ziel hinaus und konnten acht Leute von unserer Idee, Idealen und Interessen überzeugen. Damit war der erste große „Grundstein“ für den „Anoxinon e.V.“ gelegt. Leider stellte sich der Erhalt der Gemeinnützigkeit schwerer als gedacht da, sodass wir sogar einen zweiten und am Ende einen dritten Entwurf der Satzung anfertigen mussten um ein „ok“ zu erhalten.

Mit der finalen Fassung waren wir dann auch endlich bereit den „Verein“ durch die Gründungsversammlung zu gründen. Also „Einladungen zur Gründungsversammlung“ samt gesetzlicher Vorlaufzeit verschickt und Schwups, am 04.11.2018 wurde aus dem Hobby ein ernstes Projekt, ein „Verein“. Dank der dort abgehaltenen Wahlen haben wir nun einen Vertretungsberechtigten Vorstand der aus Christopher Sven Bodtke (SkyfaR) als 1. Vorsitzender, Cedric B. (Cebeer) als 2. Vorsitzender und Thomas Leister (Thomas) als Vorstandsmitglied besteht.
Soviel vorerst zu der Entstehungsgeschichte.

Künftig möchten wir unser Hauptaugenmerk auf die Aufklärungsarbeit in Form von Schrift, VOD, Audio und Ähnlichem Inhalten legen, insbesondere bei den Themen Datensicherheit, Verschlüsselung, Datenschutz, Kryptographie und Zensur. Darunter fallen natürlich auch Begleitthemen wie Open Source.

Als zweite Kernthematik bieten wir Open Source Dienste an welche den Fokus auf Datenschutz sowie Sicherheit legen. Damit wollen wir unter anderem unseren eigenen Beitrag zu dezentralen Netzwerken leisten. Derzeit gehören ein eigener XMPP Server auf Basis von Prosody, unsere Mastodon Instanz sowie der eigene VoIP Mumble Server dazu. Im späteren Verlauf des Vereins möchten wir noch andere  Dienste wie z.B. Peertube anbieten um auch hier eine klare Alternative zu schaffen.

Natürlich werden wir versuchen euch hierüber so gut wie möglich auf den laufenden zu halten und in die Entwicklung sowie Gestaltung mit einzubeziehen. Wir denken das es selbstverständlich ist dass ein solches Vorhaben nicht von nur einer Handvoll Leute bewältigt werden kann. Aus diesem Grund sind wir immer auf der Suche nach interessierten Mitstreitern die uns tatkräftig Unterstützen wollen. Schreibt uns doch einfach [hier](https://anoxinon.de/kontakt/) mal an, falls ihr Interesse haben solltet. :)

---

###### Webseite

Nachdem bereits mehrmals nachgefragt wurde, möchten wir an dieser Stelle erklären wieso unsere Webseite noch den Stand vor der Vereinsgründung hat und kaum bis keine Informationen zum Verein zur Verfügung stehen.

Der Grund ist eigentlich ganz simpel: Wir möchten keine großen Versprechen oder Ankündigungen veröffentlichen, die wir am Ende nicht halten können.

Das Abhalten einer Gründungsversammlung bedeutet zwar einen „Verein“ gegründet zu haben (rein rechtlich betrachtet), jedoch nicht dass dieser auch bereits eingetragen ist oder gar die Möglichkeit eines Beitritts oder aktiven Beteiligung daran besteht. Wir sind noch lange nicht fertig mit allen, arbeiten aber stetig und engagiert daran euch so viele Informationen zu geben wie möglich. Eine „Gründungsversammlung“ bedeutet aber auch dass eine geplante Satzung, ebenso wie die Mitgliedsbeiträge und weitere Punkte nochmals diskutiert werde. Ein Verein ist keine Diktatur, sondern lebt von einem gemeinsamen Miteinander. Dazu zählen auch die offenen Gespräche über sämtliche Punkte die diesen betreffen. Inhalte wie Beitragsordnung, Mitgliedsanträge und Satzung vor einer solchen Versammlung zur Verfügung zu stellen wäre also, aus unserer Sicht, unsinnig gewesen, da noch Änderungen möglich gewesen sind.

**Zusammengefasst war das Motto also: Weniger ist mehr und alles mit der Zeit! ;)**

Wir werden in den nächsten Tagen und Wochen die Webseite natürlich entsprechend umgestalten, aktualisieren und alle genannten Dokumente zur Verfügung stellen. Etwas Zeit bis zur offiziellen Betriebsaufnahme bleibt uns ja noch.

---

###### Dienste, Änderungen der Nutzungsbedingungen und Datenschutzerklärungen

Sämtliche Dienste die wir bereits vor Vereinsgründung bereitgestellt haben, wollen wir weiterhin betreiben. Die Planungen für weitere Angebote bleiben aufrecht.

Mit dem Wechsel der Betreiber, von Privatpersonen auf einen Verein als eigenständige Rechtsperson, müssen natürlich auch die Nutzungsbedingungen und unsere Datenschutzerklärungen angepasst werden.

Man könnte zum Beispiel aus der, eingebauten, Mastodon Datenschutzerklärung eine potenzielle 12-monatige Speicherung von Webserverlogs ableiten. Natürlich ist das nicht notwendig und wir tun dies auch nicht. Wir werden auch die anderen Bestimmungen und Erklärungen überprüfen, um in Zukunft Rechtssicherheit zu schaffen, für alle Beteiligte, um Missverständnissen durch unklare Formulierungen/Übersetzungsfehlern zu umgehen.

Selbstverständlich erhalten alle Nutzer, die einen Account bei einem unserer Dienste besitzen, eine Mitteilung. **Diese wird 2-3 Wochen vor dem Inkrafttreten der neuen Bestimmungen und dem Wechsel der Betreiber versendet.** Inkludiert sind dann natürlich alle Änderungen und Links zu den neuen Versionen.

---

Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Grüße,

euer Anoxinon Team
