+++
title = "Anoxinon Share"
description = "Anoxinon Share - File Uploader"
weight = 4
draft = false
bref = "Anoxinons hauseigener Upload Service"
+++

### Was ist Anoxinon Share?

Anoxinon Share ist ein Dienst, bei dem ihr Dateien mit einer Größe von bis zu 50 MB hochladen könnt. Daraufhin wird ein Link generiert, über den die Datei heruntergeladen werden kann. Den könnt ihr dann an beliebige Personen versenden. Die Dateien werden bei uns direkt auf dem Server verschlüsselt und nach 24 Stunden automatisch gelöscht. Zusätzlich kann ein Passwort gesetzt werden, um vor unerwünschten Downloads zu schützen oder der Link optional nach dem ersten Download automatisch als ungültig erklärt werden.

<a class="btn-mini" href="https://share.anoxinon.de/">Zu Anoxinon Share</a>
