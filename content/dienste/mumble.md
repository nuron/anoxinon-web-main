+++
description = "Community Kommunikationsplattform"
title = "Mumble"
draft = false
weight = 6
bref= "Die Kommunikationsplattform für dich und mich"
toc = true
+++

### Was ist Anoxinon Mumble?

Anoxinon Mumble ist eine freie, quelloffene und datenschutzfreundliche Sprachkonferenzsoftware recht ähnlich zu dem bekannteren TeamSpeak und steht der Allgemeinheit zur Nutzung zu Verfügung. Um Mumble zu nutzen, musst du dir das Mumble-Programm für dein Betriebssystem herunterladen.

Für Linux ist Mumble oftmals normal in der Paketverwaltung verfügbar, für andere Betriebssysteme kann man Mumble von der [Mumble Website](https://www.mumble.info) [herunterladen](https://www.mumble.info/downloads/). Mehr Infos zur Installation finden sich auch im [Mumble Wiki](https://wiki.mumble.info/wiki/Installing_Mumble).

In Mumble kannst du beispielsweise mit Freunden zusammenkommen und gemeinsam zocken, entspannen oder dich einfach zum Plausch treffen. Dir stehen dabei einige vorgefertigte Channels zu Verfügung und auf Wunsch erstellen wir dir auch gerne einen privaten Channel, sofern dieser aktiv genutzt wird.

Solltest du also einen verlässlichen und stabilen Mumble Server suchen, auf dem du immer gern gesehen bist, dann scheue dich nicht, vorbeizukommen.

* Mumble-Server-Adresse: [mumble.anoxinon.de](mumble://mumble.anoxinon.de/)
