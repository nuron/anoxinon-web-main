+++
title = "Anoxinon Social"
description = "Mastodon Instanz"
weight = 1
draft = false
bref = "Anoxinons hauseigene Mastodon Instanz im Fediverse"
+++

### Was ist Anoxinon Social?

Anoxinon Social ist die Mastodon Instanz des Vereins, die für die Allgemeinheit zur Vefügung steht. Dabei handelt es sich um ein freies, quelloffenes und datenschutzfreundliches soziales Netzwerk. Das Ganze ist dabei im Microblogging-Format gestaltet, ähnlich wie Twitter. Es können also kurze Textbeiträge, Bilder, Videos, Audios und Umfragen erstellt werden. Man kann anderen Nutzer:innen folgen, auf deren Beiträge antworten oder deren Beiträge teilen und favorisieren. Anoxinon Social kennzeichnet sich durch folgenden Merkmale:

* Beiträge bis 500 Zeichen
* Kein Tracking und keine Analyse von Daten zu Werbezwecken oder der kommerziellen Profilbildung
* Dezentral & Föderal
* Freundliche Community
* Erreicht über 4 Millionen Nutzer:innen

<a class="btn-mini" href="https://social.anoxinon.de">Zu Mastodon</a> |
<a class="btn-mini" href="https://halcyon.anoxinon.de/">Zu Halcyon</a>

### Was ist Mastodon & das Fediverse?

[Mastodon](https://joinmastodon.org) ist ein soziales Netzwerk im Microblogging-Format und ähnelt damit von den Funktionen im Groben Twitter. Im Gegensatz dazu ist Mastodon allerdings eine freie Software und kann prinzipiell von jeder:m selbst betrieben werden. Deshalb gibt es viele verschiedene Anbieter:innen, bei denen man sich ein Mastodon-Konto anlegen kann. Die verschiedenen Anbieter:innen werden auch als Mastodon-Instanzen bezeichnet. Mastodon wird also dezentral betrieben. Das besondere dabei: Alle Mastodon-Instanzen sind untereinander verbunden - sie föderieren. Das heißt, grundsätzlich ist es egal, auf welcher Instanz bestimmte Nutzer:innen sind: Auf Anoxinon Social findet und sieht man auch andere Nutzer:innen und deren Beiträge, und Nutzer:innen von anderen Instanzen sehen dein Konto und deine Beiträge. Doch damit nicht genug - nicht nur die Mastodon-Instanzen sind miteinander verbunden, sondern auch viele andere soziale Netzwerke sind mit Mastodon kompatibel, wie zum Beispiel [PeerTube](https://joinpeertube.org/) oder [Pixelfed](https://pixelfed.org). Stell dir einfach vor, du könntest als Twitter-Nutzer:in auch deinen Lieblings-YouTuber:innen oder deinen Freunden auf Instagram folgen!

**Klingt kompliziert?** Ist es nicht! Wir raten dir, probiere es einfach mal aus. Wenn du nicht zurechtkommst, sind bei Anoxinon Social so gut wie immer freundliche Nutzer:innen online, die gerne weiterhelfen. Es ist ein Blick über den Tellerrand, der es Wert ist.

### Was ist Halcyon?

[Halcyon](https://www.halcyon.social/) ist eine alternative Benutzeroberfläche für Mastodon, die sich stark an Twitter orientiert. Wenn du die Twitter-Benutzeroberfläche bevorzugst, kannst du Anoxinon Social mit Halcyon nutzen und so das Beste von beiden Welten vereinen.
